﻿using System;

namespace YANDRYBRIONES3A
{
    class EJERCICIO10
    {
        public static void Main(string[] args)
        {
            int num;
            int opcion = 0;
            do
            {
                Console.Clear();
                Console.WriteLine("1. Salir");
                Console.WriteLine("2. Sumatorio");
                Console.WriteLine("3. Factorial");  
                opcion = Convert.ToInt32(Console.ReadLine());
                Console.Clear();
                switch (opcion)
                {
                    case 2:
                        do
                        {
                            Console.Write("Por favor Digite un número entero positivo: ");
                            num = int.Parse(Console.ReadLine());
                            verificar1(num);
                        } while (num < 0);
                        sumatoria(num);
                        Console.ReadKey();
                        static void verificar1(int n)
                        {
                            if (n < 0)
                            {
                                Console.Write("\nError: No se permite números negativos...\n");
                            }
                        }
                        static void sumatoria(int n)
                        {
                            int suma = 0;
                            for (int i = 1; i <= n; i++)
                            {
                                suma = suma + i;
                            }
                            Console.WriteLine("\nLa Sumatoria de " + n + " es: " + suma);
                        }
                        break;
                    case 3:
                        do
                        {
                            Console.Write("Por favor Digite un número entero positivo: ");
                            num = int.Parse(Console.ReadLine());
                            verificar(num);
                        } while (num < 0);
                        factorial(num);
                        Console.ReadKey();
                        static void verificar(int n)
                        {
                            if (n < 0)
                            {
                                Console.Write("\nError: No existe el factorial de un número negativo...\n");
                            }
                        }
                        static void factorial(int n)
                        {
                            int fact = 1;
                            for (int i = 1; i <= n; i++)
                            {
                                fact *= i;
                            }
                            Console.WriteLine("\nEl Factorial de " + n + " es: " + fact);
                        }
                        break;
                }
            } while (opcion != 1);
            Console.WriteLine("ADIOS");
            Console.ReadKey();
        }                  
    }
}