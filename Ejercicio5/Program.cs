﻿using System;

namespace YANDRYBRIONES3A
{
    class EJERCICIO5
    {
        static void Main(string[] args)
        {
            int n, linea, fila, ast;
            Console.Write("Digite el tamaño de la piramide en lineas: ");
            n = Convert.ToInt32(Console.ReadLine());
            for (linea = 1; linea <= n; linea++)
            {
                for (fila = 0; fila < n- linea; fila++)
                {
                    Console.Write(" ");
                }
                for (ast = 0; ast < (linea * 2)-1; ast++)
                {
                    Console.Write("+");
                }
                Console.WriteLine(); 
            }
            Console.ReadKey();
        }
    }
}
