﻿using System;

namespace YandryBriones3A
{
	class EJERCICIO4
	{

		static void Main(string[] args)
		{
			double horas;
			double multi;
			double resta;
			double suma;
			String continuar;
			horas = 0;
			multi = 0;
			resta = 0;
			suma = 0;
			do
			{
				Console.WriteLine("Ingrese sus horas trabajadas");
				horas = Double.Parse(Console.ReadLine());
				if (horas > 35)
				{
					resta = horas - 35;
					multi = resta * 22;
					suma = 525 + multi;
					Console.WriteLine("Su sueldo a cobrar es:");
					Console.WriteLine(suma);
				}
				else
				{
					multi = horas * 15;
					Console.WriteLine("Su sueldo a cobrar es de:");
					Console.WriteLine(multi);
				}
				Console.WriteLine("¿Desea realizar otro calculo?");
				Console.WriteLine("SI/NO");
				continuar = Console.ReadLine();
				Console.WriteLine("\n");
				Console.Clear();
			} 
			while (continuar != "no");
			Console.ReadKey();
			Console.WriteLine("Hasta Pronto :)");
			Console.ReadKey();
		}

	}

}