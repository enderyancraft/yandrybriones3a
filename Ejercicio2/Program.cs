﻿using System;

namespace YANDRYBRIONES3A
{
	class EJERCICIO2
	{

		static void Main(string[] args)
		{
			int a;
			int b;
			double n;
			Console.WriteLine("Escribir un numero");
			n = Double.Parse(Console.ReadLine());
			for (a = 1; a <= n; a++)
			{
				for (b = 1; b <= n; b++)
				{
					if (a > 1 && a < n && b > 1 && b < n)
					{
						Console.Write("  ");
					}
					else
					{
						Console.Write("* ");
					}
				}
				Console.WriteLine("");
			}
			Console.ReadKey();
		}

	}

}
